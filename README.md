# ir2oct

Package with useful OCTAVE scripts, functions, colormaps, etc

## Installation

To install the latest release run the following from within OCTAVE

```matlab
pkg install "https://gitlab.com/ir2-lab/ir2oct/-/archive/main/ir2oct-main.tar.gz"
```

## Contents

- ``inst/cryo/`` - functions for cryogenic calculations (thermal properties of materials, Al, Cu, Teflon, etc)
- ``inst/colormaps`` - various alternative colormaps from `matplotlib`
- ``inst/print/`` - functions for printing publication-ready figures
- Other utility functions: 
  - `temp2emf.m, emf2temp.m` convert thermocouple emf to temperature and vice versa
  - `linefit.m` complete with errors, correlation and GOF
  - `bloch_res.m` Bloch-Grueneisen phonon resistivity. See also [BlochGruneisen.ipynb](./BlochGruneisen.ipynb)

Function documentation can be found in https://ir2-lab.gitlab.io/ir2oct/.

