#!/bin/bash

prefix="ir2oct/"
filename="ir2oct.tar"

git archive --format=tar --prefix=$prefix --output=$filename HEAD 
gzip --force --verbose $filename
