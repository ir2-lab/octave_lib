# -*- texinfo -*-
# @deftypefn  {Function File} { @var{c} = } copper_c(@var{T})
# 
# Returns the specific heat of copper in J/kg-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = copper_c(T)
% function c = copper_c(T)
%
% Returns the specific heat of copper in J/kg-K at temperature T in K
% From NIST Cryogenic Technologies Group
% http://cryogenics.nist.gov/MPropsMAY/OFHC%20Copper/OFHC_Copper_rev.htm

t=log10(T(:));

A = [-1.91844
-0.15973
8.61013 
-18.996 
21.9661 
-12.7328
3.54322 
-0.3797 ];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*A(8)))))));
c = 10.^c;
c = reshape(c,size(T));