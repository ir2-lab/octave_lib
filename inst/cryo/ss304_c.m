# -*- texinfo -*-
# @deftypefn  {Function File} { @var{c} = } ss304_c(@var{T})
# 
# Returns the specific heat of stainless steel 304 in J/kg-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = ss304_c(T)

t=log10(T(:));

A = [22.0061 
-127.5528
303.647 
-381.0098
274.0328 
-112.9212
24.7593 
-2.239153];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*A(8)))))));
c = 10.^c;
c = reshape(c,size(T));