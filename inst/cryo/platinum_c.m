# -*- texinfo -*-
# @deftypefn  {Function File} { @var{c} = } platinum_c(@var{T})
# 
# Returns the specific heat of platinum in J/kg-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = platinum_c(T)

idx = find(T<4);
T(idx)=4;
t=log10(T(:));

A = [-1.6135538 
0.95823584 
1.4317770 
-3.5963989 
5.1299735 
-2.4186452 
-0.12560841 
0.34342394 
-0.06198179 ];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*(A(8)+t.*A(9))))))));
c = 10.^c;
c = reshape(c,size(T));