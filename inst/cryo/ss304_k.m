# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } ss304_k(@var{T})
# 
# Returns the thermal conductivity of stainless steel 304 in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = ss304_k(T)

idx = find(T<4);
T(idx)=4;
t=log10(T(:));

A = [-1.4087
1.3982 
0.2543 
-0.6260
0.2334 
0.4256 
-0.4658
0.1650 
-0.0199];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*(A(8)+t.*A(9))))))));
c = 10.^c;
c = reshape(c,size(T));