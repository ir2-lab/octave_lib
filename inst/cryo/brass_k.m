# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } brass_k(@var{T})
# 
# Returns the thermal conductivity of brass in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function k = brass_k(T)

t=log10(T(:));

A = [  0.021035 
 -1.01835 
 4.54083 
 -5.03374 
 3.20536 
 -1.12933 
 0.174057 
 -0.0038151 ];

k = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*A(8)))))));
k = 10.^k;
k = reshape(k,size(T));