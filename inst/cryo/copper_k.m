# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } copper_k(@var{T},@var{RRR})
# 
# Returns the thermal conductivity of copper in W/m-K at temperature T in K.
#
# RRR=1..5 is a purity index (defaults to 1)
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function k = copper_k(T,RRR)

t=T(:);
n=length(t);
tsq=sqrt(t);
tt=[ones(n,1) tsq t t.*tsq t.*t];

if nargin>1, j = RRR; else j = 1; end


B = [1 1 1 1 1;...
    -0.41538 -0.47461 -0.4918 0.3981 -0.54074; ... 
    0.13294 0.13871 0.13942 -0.1346 0.15362; ... 
    -0.0219 -0.02043 -0.019713 0.01342 -0.02105; ... 
    0.0014871 0.001281 0.0011969 0.0002147 0.0012226]; 

A = [1.8743 2.2154 2.3797 1.357 2.8075; ... 
    -0.6018 -0.88068 -0.98615 2.669 -1.2777; ...
    0.26426 0.29505 0.30475 -0.6683 0.36444; ...
    -0.051276 -0.04831 -0.046897 0.05773 -0.051727; ...
    0.003723 0.003207 0.0029988 0 0.0030964]; 

k = (tt*A(:,j))./(tt*B(:,j));
k = 10.^k;
k = reshape(k,size(T));