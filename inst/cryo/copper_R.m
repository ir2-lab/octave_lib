# -*- texinfo -*-
# @deftypefn  {Function File} { @var{R} = } copper_R(@var{T})
# 
# Returns the resistivity of copper in Ohm-m at temperature T in K.
#
# @end deftypefn

function R = copper_R(T)
% Returns resistivity of copper in Ohm-m

p = [ 0.1181714
-2.387543
 18.59075
-69.24609
 124.0291
-108.7164];

p = [
  0.006307104
 -0.2289753
   3.480022
  -28.59133
   136.5139
  -376.8804
   555.7915
  -360.5014];


R=exp(polyval(p,log(T)))+5e-10;

%%
% clear
% T1=10:10:70;
% R1=3.33e-6*T1.^3+0.1;
% T2=100:50:300;
% R2=5.02e-2*T2-2.22;
% T=[T1 T2];
% R=[R1 R2];
% R=R/R(end)*1.6e-8;
% p=polyfit(log(T),log(R),5)
% Ro=exp(polyval(p,log(273)));
% R200=Ro*(1+4.3e-3*200);
% R=[R R200 10e-8 ];
% T=[T 473 1400];
% p=polyfit(log(T),log(R),7)
% loglog(T,R,'.',10:10:1400,exp(polyval(p,log(10:10:1400))))
% 
% 
