# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } polyimide_k(@var{T})
# 
# Returns the thermal conductivity of polyimide (Kapton) in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function k = polyimide_k(T)

t=log10(T(:));

A = [  5.73101  
       -39.5199 
       79.9313 
       -83.8572 
       50.9157 
       -17.9835 
       3.42413 
       -0.27133 ];

k = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*A(8)))))));
k = 10.^k;
k = reshape(k,size(T));
