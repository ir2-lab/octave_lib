# -*- texinfo -*-
# @deftypefn  {Function File} { @var{c} = } al6061_c(@var{T})
# 
# Returns the specific heat of Al 6061 in J/kg-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = al6061_c(T)

t=log10(T(:));

A = [46.6467 
-314.292
866.662 
-1298.3 
1162.27 
-637.795
210.351 
-38.3094
2.96344];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*(A(8)+t.*A(9))))))));
c = 10.^c;
c = reshape(c,size(T));