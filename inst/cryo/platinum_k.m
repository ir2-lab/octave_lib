# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } platinum_k(@var{T})
# 
# Returns the thermal conductivity of platinum in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = platinum_k(T)
% function c = platinum_k(T)
%
% Returns the thermal conductivity of platinum in W/m-K at temperature T in K
% From NIST Cryogenic Technologies Group
% http://cryogenics.nist.gov

idx = find(T<4);
T(idx)=4;
t=log10(T(:));

A = [ -7.33450054
80.8550484 
-268.441084 
481.629105 
-503.890454 
314.812622 
-115.699394 
23.0957119 
-1.93361717 
];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*(A(8)+t.*A(9))))))));
c = 10.^c;
c = reshape(c,size(T));