# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } al6061_k(@var{T})
# 
# Returns the thermal conductivity of Al 6061 in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function c = al6061_k(T)

t=log10(T(:));

A = [0.07981 
1.0957 	
-0.07277
0.08084 
0.02803 
-0.09464
0.04179 
-0.00571];

c = A(1) + t.*(A(2) + t.*(A(3) + t.*(A(4) + t.*(A(5) + t.*(A(6) + t.*(A(7) + t.*A(8)))))));
c = 10.^c;
c = reshape(c,size(T));