# -*- texinfo -*-
# @deftypefn  {Function File} { @var{k} = } teflon_k(@var{T})
# 
# Returns the thermal conductivity of teflon in W/m-K at temperature T in K.
#
# From NIST Cryogenic Technologies Group http://cryogenics.nist.gov
#
# @end deftypefn

function k = teflon_k(T)

A = [2.7380 
-30.677
89.430 
-136.99
124.69 
-69.556
23.320 
-4.3135
0.33829];

t=log10(T(:));


k = A(1) + t.*( ...
    A(2) + t.*( ...
    A(3) + t.*( ...
    A(4) + t.*( ...
    A(5) + t.*( ...
    A(6) + t.*( ...
    A(7) + t.*( ...
    A(8) + t.*A(9))))))));
k = 10.^k;
k = reshape(k,size(T));