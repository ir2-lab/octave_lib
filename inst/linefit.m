# -*- texinfo -*-
# @deftypefn {Function File} { [@var{a},@var{b},@var{da},@var{db},@var{chi2},@var{Q}] = } linefit(@var{xdata},@var{ydata},@var{dydata})
# @deftypefnx  {Function File} { [@var{a},@var{b},@var{da},@var{db},@var{chi2},@var{Q}] = } linefit(@var{xdata},@var{ydata})
# 
# Linear fit of y=a+b*x  to the data.
#
# @var{xdata},@var{ydata} are two vectors with the observations.
# @var{dydata} is the error of @var{ydata} (optional).
#
# The function calculates the parameters @var{a},@var{b} and 
# their estimation errors @var{da} and @var{db}, respectively.
#
# If @var{dydata} is given then the @var{chi2} (sum of squares 
# (y-a-b*x).^2 ) and the goodness-of-fit parameter @var{Q} have 
# meaningfull values.
#
# Algorithm: as described in Press et al. "Numerucal Recipes"
# @end deftypefn

function [a,b,siga,sigb,chi2,Q]=linefit(x,y,dy)

ndata = length(x);

if nargin<3,
    sx = sum(x);
    sy = sum(y);
    ss=ndata;
    sxoss=sx/ss;
    t=x-sxoss;
    st2=sum(t.^2);
    b = sum(t.*y);
    b=b/st2;
    a=(sy-sx*b)/ss;
    siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
    sigb=sqrt(1.0/st2); 
    chi2=sum((y-a-b*x).^2);
    if (ndata > 2) sigdat=sqrt(chi2/(ndata-2));
    else sigdat=1;
    end
    siga=siga*sigdat;
    sigb=sigb*sigdat;
    Q=0;
else
    wt=1./dy.^2;
    ss = sum(wt);
    sx = sum(x.*wt);
	sy = sum(y.*wt);
    sxoss=sx/ss;
    t = (x-sxoss)./dy;
    st2 = sum(t.^2);
    b = sum(t.*y./dy);
    b=b/st2;
    a=(sy-sx*b)/ss;
    siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
    sigb=sqrt(1.0/st2); 
    chi2=sum((y-a-b*x).^2.*wt);
    if (ndata>2) %Q=gam.gammq(0.5*(ndata-2),0.5*chi2);
        Q=1-gammainc(0.5*(ndata-2),0.5*chi2,'lower');
    end
    chi2 = chi2/(ndata-2);
end