# -*- texinfo -*-
# @deftypefn {Function File} { @var{y} = } bloch_res(@var{x})
# 
# The Bloch-Grueneisen formula for the temperature dependent
# resistivity of a metal.
#
# @var{x} is an array of values corresponding to T/Θ_D, where
# T is the temperature and Θ_D is the Debye temperature. bloch_res
# returns a numerical approximation to the function
#
#   y(x) = x^5 * int_0^(1/x) t^5 [(1-e^(-t))(e^t-1)]^(-1) dt
#
# which describes the phonon resistivity of a simple metal as a function
# of temperature.
#
# Algorithm: see the file 
# https://gitlab.com/ir2-lab/ir2oct/-/blob/main/BlochGrueneisen.ipynb?ref_type=heads
#
# @end deftypefn

function y=bloch_res(x)

  y=zeros(size(x));
  % low T, abs error < 10^-13
  i = find(x<=0.03333);
  y(i) = x(i).^5*124.431330617; % 124.431330617 = 120*zeta(5)
  % high T, abs error < 10^-11
  i = find(x>=20); 
  y(i)=x(i)/4-(1/72-(1/1920)./x(i).^2)./x(i);
  % intermediate T
  i = find(x>0.03333 & x<20);
  y(i) = x(i).^5.*bloch_integral(1./x(i));

end

function y = bloch_integral(x)
  ifunc = @(x) x.^5.*exp(-x)./(1-exp(-x)).^2;
  y=zeros(size(x));
  for i=1:length(x)
    y(i) = quad(ifunc,0,x(i));
  end
end

