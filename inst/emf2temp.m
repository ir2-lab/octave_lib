# -*- texinfo -*-
# @deftypefn  {Function File} { @var{T} = } temp2emf(@var{emf},@var{type})
# 
# Return the temperature corresponding to a termocouple emf.
#
# The emf is given with reference to 0°C.
#
# The values are calculated based on approximate analytical expressions
# given by NIST:
#
# Karen Garrity (2000), NIST ITS-90 Thermocouple Database - SRD 60, https://doi.org/10.18434/mds2-48
#
# https://its90.nist.gov/
#
# Input:
# @itemize @minus
# @item
# @var{emf}: The thermocouple emf in mV. Scalar or matrix.
# @item
# @var{type}: Thermocouple letter code. Currently only 'K' and 'T' types are supported
# @end itemize
#
# Output:
# @itemize @minus
# @item
# @var{T}: The temperature in °C. Same size as T.
# @end itemize
#
# @end deftypefn

function T = emf2temp(emf,type)

if nargin<2, type = 'T'; end

To = 273.15;

if type=='T',
emf0 = -6.18;
emf1 = -5.603;
emf2 = 20.872;

idx_1 = find(emf<emf0);
idx0 = find(emf>=emf0 & emf<emf1);
idx1 = find(emf>=emf1 & emf<0);
idx2 = find(emf>=0 & emf<=emf2);
idx3 = find(emf>emf2);

p_1 = [...
 -31604.220323
-3420.25548912
-157.773708512
 6.20533022422
             0];
p0 = [...
  1.17275043432
 -25.0963534639
 -35.5182449623
 -20.0345261816
 -4.90388896489
-0.967631341136
  1.24562996076
              0];    
p1 = [...
0.0012668171
 0.020241446
  0.13304473
  0.42527777
  0.79018692
 -0.21316967
   25.949192
           0];
p2 = [...
-7.293422e-007
 6.048144e-005
  -0.002165394
    0.04637791
    -0.7602961
        25.928
             0];

T = zeros(size(emf));
T(idx_1) = -250 + 20*polyval(p_1,emf(idx_1)-emf0);
T(idx0) = -200 + 50*polyval(p0,emf(idx0)-emf1);
T(idx1) = polyval(p1,emf(idx1));
T(idx2) = polyval(p2,emf(idx2));
dTdv = polyval((6:-1:1)'.*p2(1:end-1),emf2);
T(idx3) = polyval(p2,emf2) + dTdv*(emf(idx3)-emf2);

elseif type=='K',
    
    idx0 = find(emf<-5.891);
    idx1 = find(emf>=-5.891 & emf<0);
    idx2 = find(emf>=0 & emf<20.644);
    idx3 = find(emf>=20.644);

p0 = [-0.59863335257
 4.01147836498
-7.89982050802
-1.75328898674
 20.9636201651
-11.7351557301
-18.5632212697
 15.8576320748
 4.97307802719
-3.92801389329
-4.59521215506
 19.0403312338
-229.810775741];
mu0=[-6.26019921532
0.172503045766];

p1 = [       0
-0.00051920577
  -0.010450598
  -0.086632643
   -0.37342377
    -0.8977354
    -1.0833638
    -1.1662878
     25.173462
             0];
p2 = [-1.052755e-008
 1.057734e-006
 -4.41303e-005
  0.0009804036
   -0.01228034
     0.0831527
    -0.2503131
    0.07860106
      25.08355
             0];
p3 = [-3.11081e-008
8.802193e-006
-0.0009650715
   0.05464731
    -1.646031
     48.30222
    -131.8058];

    T = zeros(size(emf));
    T(idx0) = polyval(p0,emf(idx0),[],mu0);
    T(idx1) = polyval(p1,emf(idx1));
    T(idx2) = polyval(p2,emf(idx2));
    T(idx3) = polyval(p3,emf(idx3));

end


