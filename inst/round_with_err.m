# -*- texinfo -*-
# @deftypefn  {Function File} { [@var{y}, @var{dy}] = } round_with_err(@var{x}, @var{dx}, @var{d})
# @deftypefnx  {Function File} { [@var{y}, @var{dy}] = } round_with_err(@var{x}, @var{dx})
# 
# Rounding values according to their error (uncertainty).
#
# Given a value @var{x} and error @var{dx} this function returns in @var{dy}
# the error rounded to @var{d} significant digits and in @var{y} the value 
# rounded to the same order as the error.
#
# E.g., the value 6.345243±0.003567 becomes 6.3452±0.0035 for d=2 and 
# 6.345±0.004 for d=1.
#
# The default value of d is 2.
#
# @var{x} and @var{dx} can be either vectors or scalars. If they
# both are vectors they must be of equal size.
#
# @end deftypefn

function [r,dr] = round_with_err(x,dx,d)

if nargin<3, d=2; end

[r,n] = frexp10(x);
[dr,dn] = frexp10(dx);

precision = n-dn+d;
i = find(precision<1);
precision(i)=1;

scal = 10.^(precision-n);
r = round(x.*scal)./scal;

scal = 10.^(d-dn);
dr = round(dx.*scal)./scal;

end

function [m,n] = frexp10(x)
  logn = log10(abs(x));
  n = floor(logn) + 1;
  m = x.*(10.^(-n));
end

