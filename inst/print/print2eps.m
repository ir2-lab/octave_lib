# -*- texinfo -*-
# @deftypefn  {Function File} {  } print2eps(@var{h}, @var{sz}, @var{fname})
# 
# Export figure to EPS file.
#
# Arguments:
# @itemize @minus
# @item
# @var{h}: figure handle (use gcf for current figure).
# @item
# @var{sz}: size in cm as a 2 element vector [width, height] 
# @item
# @var{fname}: filename as a string without the extension
# @end itemize
#
# @end deftypefn

function print2eps(h,sz,fname)

set(h,'PaperUnits','centimeters')
set(h,'PaperSize',sz)
set(h,'PaperPosition',[0 0 sz(1) sz(2)])

print(h,'-depsc','-loose',fname)