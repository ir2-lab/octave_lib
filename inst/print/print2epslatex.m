# -*- texinfo -*-
# @deftypefn  {Function File} {  } print2epslatex(@var{h}, @var{sz}, @var{fname})
# 
# Export figure to EPS using latex for the text portions.
#
# A LaTeX processor must be installed in your system.
#
# Arguments:
# @itemize @minus
# @item
# @var{h}: figure handle (use gcf for current figure).
# @item
# @var{sz}: size in cm as a 2 element vector [width, height] 
# @item
# @var{fname}: filename as a string without the extension
# @end itemize
#
# @end deftypefn

function print2epslatex(h,sz,fname)

set(h,'PaperUnits','centimeters')
set(h,'PaperSize',sz)
set(h,'PaperPosition',[0 0 sz(1) sz(2)])

% print the figure = create latex and eps files
print(h,fname,'-depslatexstandalone');

% latex the tex file
[out, txt] = system(['latex -halt-on-error ' fname '.tex']);
if out~=0,
  error('Error running latex. See log file.')
endif

% convert the dvi to eps
[out, txt] = system(['dvips -o ' fname '.eps ' fname '.dvi']);

% delete all intermediate files
del_comm = '';
if ispc, del_comm = 'del '; else del_comm = 'rm '; endif
[out, txt] = system([del_comm fname '.tex ' ...
  fname '.log ' fname '.dvi ' fname '.aux ' ...
  fname '-inc.eps']); 