# -*- texinfo -*-
# @deftypefn  {Function File} {  } print2pdflatex(@var{h}, @var{sz}, @var{fname})
# 
# Export figure to PDF using latex for the text portions.
#
# A LaTeX processor must be installed in your system.
#
# Arguments:
# @itemize @minus
# @item
# @var{h}: figure handle (use gcf for current figure).
# @item
# @var{sz}: size in cm as a 2 element vector [width, height] 
# @item
# @var{fname}: filename as a string without the extension
# @end itemize
#
# @end deftypefn


function print2pdflatex(h,sz,fname)

set(h,'PaperUnits','centimeters')
set(h,'PaperSize',sz)
set(h,'PaperPosition',[0 0 sz(1) sz(2)])

print(h,'-dpdflatexstandalone',fname)

[out, txt] = system(['pdflatex -halt-on-error ' fname '.tex']);

# for use under flatpak
#[out, txt] = system(['flatpak-spawn --host pdflatex -halt-on-error ' fname '.tex']);

if out~=0,
  error('Error running pdflatex. See log file.')
endif

% delete all intermediate files
del_comm = '';
if ispc, del_comm = 'del '; else del_comm = 'rm '; endif

[out, txt] = system([del_comm fname '.tex ' fname '.log ' fname '.aux ' ...
  fname '-inc.pdf']);
