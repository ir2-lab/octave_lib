# -*- texinfo -*-
# @deftypefn  {Function File} { @var{emf} = } temp2emf(@var{T},@var{type})
# 
# Return the nominal emf for a thermocouple of a given type.
#
# The emf is given with reference to 0°C.
#
# The values are calculated based on approximate analytical expressions
# given by NIST:
#
# Karen Garrity (2000), NIST ITS-90 Thermocouple Database - SRD 60, https://doi.org/10.18434/mds2-48
#
# https://its90.nist.gov/
#
# Input:
# @itemize @minus
# @item
# @var{T}: Temperature in °C. Scalar or matrix.
# @item
# @var{type}: Thermocouple letter code. Currently only 'K' and 'T' types are supported
# @end itemize
#
# Output:
# @itemize @minus
# @item
# @var{emf}: The thermocouple emf in mV. Same size as T.
# @end itemize
#
# @end deftypefn

function [emf, dVdT] = temp2emf(T,type)

if nargin<2, type = 'K'; end

if type=='T',

# ************************************
# * This section contains coefficients for type T thermocouples for
# * the two subranges of temperature listed below.  The coefficients 
# * are in units of °C and mV and are listed in the order of constant 
# * term up to the highest order.   The equation is of the form 
# * E = sum(i=0 to n) c_i t^i.
# *
# *     Temperature Range (°C)
# *       -270.000 to 0.000 
# *        0.000 °C to 400.000
# ************************************
# name: reference function on ITS-90
# type: T
# temperature units: °C
# emf units: mV
# range 1: -270.000, 0.000, 14
# range 2: 0.000, 400.000, 8

idx1 = find(T<0);
idx2 = find(T>=0 & T<=400);

p1 = [7.9795153927e-031
1.3945027062e-027
 1.079553927e-024
4.8768662286e-022
1.4251594779e-019
2.8213521925e-017
3.8493939883e-015
3.6071154205e-013
2.2651156593e-011
9.0138019559e-010
2.0032973554e-008
1.1844323105e-007
4.4194434347e-005
   0.038748106364
                0];
p2 = [-2.7512901673e-020
  4.547913529e-017
-3.0815758772e-014
 1.0996880928e-011
-2.1882256846e-009
 2.0618243404e-007
  3.329222788e-005
    0.038748106364
                 0];

emf = zeros(size(T));
emf(idx1) = polyval(p1,T(idx1));
emf(idx2) = polyval(p2,T(idx2));

if nargout>1,
    dVdT(idx1) = polyval((14:-1:1)'.*p1(1:end-1),T(idx1));
    dVdT(idx2) = polyval((8:-1:1)'.*p2(1:end-1),T(idx2));
end

elseif type=='K'

# ************************************
# * This section contains coefficients for type K thermocouples for
# * the two subranges of temperature listed below.  The coefficients 
# * are in units of °C and mV and are listed in the order of constant 
# * term up to the highest order.  The equation below 0 °C is of the form 
# * E = sum(i=0 to n) c_i t^i.
# *
# * The equation above 0 °C is of the form 
# * E = sum(i=0 to n) c_i t^i + a0 exp(a1 (t - a2)^2).
# *
# *     Temperature Range (°C)
# *        -270.000 to 0.000 
# *         0.000 to 1372.000
# ************************************
# name: reference function on ITS-90
# type: K
# temperature units: °C
# emf units: mV
# range 1: -270.000, 0.000, 10
# range 2: 0.000, 1372.000, 9    
    
    idx1 = find(T<0);
    idx2 = find(T>=0);

p1 = [-1.6322697486e-023
-1.9889266878e-020
-1.0451609365e-017
-3.1088872894e-015
-5.7410327428e-013
-6.7509059173e-011
-4.9904828777e-009
-3.2858906784e-007
 2.3622373598e-005
    0.039450128025
                 0];
p2 = [-1.2104721275e-026
 9.7151147152e-023
-3.2020720003e-019
 5.6075059059e-016
-5.6072844889e-013
 3.1840945719e-010
-9.9457592874e-008
 1.8558770032e-005
    0.038921204975
   -0.017600413686];

a =[0.118597600000E+00
 -0.118343200000E-03
  0.126968600000E+03];


    emf = zeros(size(T));
    emf(idx1) = polyval(p1,T(idx1));
    emf(idx2) = polyval(p2,T(idx2)) + a(1)*exp(a(2)*(T(idx2)-a(3)).^2);

end
